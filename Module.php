<?php

namespace app\modules\wolfgroup;

use Yii;

/**
 * wolfgroup module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wolfgroup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        Yii::configure($this, require __DIR__ . '/config.php');
        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\wolfgroup\commands';
        }
    }
}
