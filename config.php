<?php
return [
    'components' => [
        'mutex' => [
            'class' => 'yii\mutex\FileMutex',
            'mutexPath' => '/tmp/mutex'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'encryption' => 'tls',
                'host' => 'smtp.office365.com',
                'port' => '587',
                'username' => 'api-notifications@wolfgroup.com.au',
                'password' => '!',
            ],             
        ],
    ],
];