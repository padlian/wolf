<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\forms\WooData;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WooController extends Controller
{
    public $listNumber=0;
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */

    public function options($actionID)
    {
        return ['listNumber'];
    }
    
    public function optionAliases()
    {
        return ['ln' => 'message'];
    }

    public function getWoo()
    {
        // public function getWoo()
        // $host = 'https://onesixeightlondon.com.au/wp-json/wc/v3/orders?status=processing,on-hold';
        $host = Yii::$app->params['wooHost'];
        $username = Yii::$app->params['wooUsername'];
        $password = Yii::$app->params['wooPassword'];
        $additionalHeaders = '';
        $ch = curl_init($host);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        curl_close($ch);
        $return = json_decode($return);
        // $datas =  $return[0];
        // return $datas;
        // print_r($return[0]->id);
        // exit;
        // return $return;
        // $woo = Yii::$container->get(WooData::class, [], $return[0]);
        $woo = new WooData($return, []);
        return $woo;
    }

    public function actionIndex()
    {
        // echo $this->listNumber;
        $woo = $this->getWoo();
        // echo $woo->wooOrder[0]->id;
        print_r($woo->maropostFormat($this->listNumber));
    }

    // 939170
    // 939169
    // 939163

    // public function actionIndex2(){
    //     $woo = $this->getWoo();

    //     $woo_data = json_decode($woo);

    //     // print_r($woo_data);
    //     var_dump($woo_data);
    // }

}
