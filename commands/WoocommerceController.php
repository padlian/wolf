<?php

namespace app\commands;

use app\components\Woocommerce;
use Yii;
use app\components\Neto;
use app\forms\woocommerce\D2uOrderMapper;
use app\forms\woocommerce\D2uProductMapper;
use app\forms\woocommerce\HkOrderMapper;
use app\forms\woocommerce\HkProductMapper;
use app\forms\woocommerce\OrderMapper;
use app\forms\woocommerce\ProductMapper;
use Carbon\Carbon;
use InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * @author Yoyon Cahyono <yoyoncahyono@gmail.com>
 */
class WoocommerceController extends BaseMutexController
{
    /**
     * @return Neto
     */
    protected function getConfig($componentName)
    {
        if (empty($this->config)) throw new InvalidArgumentException('config option must be set');
        $configs = [
            'd2u' => [
                'neto' => [
                    'siteUrl' => 'https://.neto.com.au/',
                    'apiKey' => '',
                    'username' => '',
                ],
                'woo' => [
                    'endpoint' => 'https://onesixeightlondon.com.au',
                    'key' => 'ck_591e90f2dc22019bd1745865d634406e264f7372',
                    'secret' => 'cs_543c3da23611162df799a813ec9c63be01e2763c',
                ],
                'ordermapper' => D2uOrderMapper::class,
                'productmapper' => D2uProductMapper::class
            ],
        ];
        $config = ArrayHelper::getValue($configs, [$this->config, $componentName]);
        if (empty($config)) {
            throw new InvalidArgumentException('Config for "' . $this->config . '/' . $componentName . '" not found');
        }
        return $config;
    }

    /**
     * @return Neto
     */
    protected function getNeto()
    {
        $netoConfig = $this->getConfig('neto');
        /**
         * @var Neto $neto
         */
        $neto = Yii::$container->get(Neto::class, [], $netoConfig);
        return $neto;
    }

    /**
     * @return Woocommerce
     */
    protected function getWoo()
    {
        $wooConfig = $this->getConfig('woo');
        /**
         * @var Woocommerce $woo
         */
        $woo = Yii::$container->get(Woocommerce::class, [], $wooConfig);
        return $woo;
    }

    /**
     * @return string
     */
    protected function getOrderMapperClass()
    {
        $orderMapperClass = $this->getConfig('ordermapper');
        if (empty($orderMapperClass) || !class_exists($orderMapperClass)) {
            $orderMapperClass = OrderMapper::class;
        }
        return $orderMapperClass;
    }

    /**
     * @return string
     */
    protected function getProductMapperClass()
    {
        $productMapperClass = $this->getConfig('productmapper');
        if (empty($productMapperClass) || !class_exists($productMapperClass)) {
            $productMapperClass = ProductMapper::class;
        }
        return $productMapperClass;
    }

    /**
     * @param array $netoProduct
     * @return ProductMapper
     */
    protected function getProductMapper($netoProduct, $wooProductId)
    {
        $productMapperClass = $this->getProductMapperClass();
        /**
         * @var ProductMapper $productMapper
         */
        $productMapper = Yii::$container->get($productMapperClass, [$netoProduct, null, $wooProductId, $this->getWoo(), $this->getNeto()]);
        return $productMapper;        
    }

    public function actionOrder($orderId = null)
    {
        /**
         * @var Neto $neto
         */
        $neto = $this->getNeto();

        /**
         * @var Woocommerce $woo
         */
        $woo = $this->getWoo();
        
        $wooOrders = [];
        $netoOrders = [];
        if (!empty($orderId)) {
            $wooOrders = $woo->getOrdersById($orderId);
        } else {            
            $wooOrders = $woo->getOrdersByStatus(['processing', 'on-hold']);
        }        
        echo date('c ') . 'Total Queue Orders: ' . count($wooOrders). PHP_EOL;
        if (empty($wooOrders)) {
            return;
        }
        $orderDateFrom = Carbon::parse(min(ArrayHelper::getColumn($wooOrders, 'date_created')))->subDay();
        $dispatchedNetoOrders = $this->getNetoOrders(null, null, $orderDateFrom, ['Dispatched']);
        if (!empty($dispatchedNetoOrders)) {
            $dispatchedNetoOrders = ArrayHelper::index($dispatchedNetoOrders, function($netoOrder) {
                return strval(ArrayHelper::getValue($netoOrder, ['PurchaseOrderNumber']));
            });
        }
        $netoOrders = $this->getNetoOrders(null, null, $orderDateFrom);
        if (!empty($netoOrders)) {
            $netoOrders = ArrayHelper::index($netoOrders, function($netoOrder) {
                return strval(ArrayHelper::getValue($netoOrder, ['PurchaseOrderNumber']));
            });
        }
        if (!empty($dispatchedNetoOrders) && !empty($netoOrders)) {
            $netoOrders = $dispatchedNetoOrders + $netoOrders;
        }
        // var_dump($netoOrders, ArrayHelper::getValue($netoOrders, '25405'));
        // die();
        foreach ($wooOrders as $wooOrder) {
            $wooOrderId = ArrayHelper::getValue($wooOrder, ['id']);
            $purchaseOrderNumber = $wooOrderId;
            if (!isset($netoOrders[$purchaseOrderNumber])) {                
                $wooNetoOrders = $this->getNetoOrders(null, $wooOrderId);
                if (!empty($wooNetoOrders[0])) {
                    $netoOrders[$purchaseOrderNumber] = $wooNetoOrders[0];
                }
            }
        }
        // var_dump($netoOrders);
        // var_dump($wooOrders);
        // die();
        // var_dump(ArrayHelper::getValue($netoOrders, '25405'));
        $orderMapperClass = $this->getOrderMapperClass();
        foreach ($wooOrders as $wooOrder) {
            $wooOrderId = ArrayHelper::getValue($wooOrder, ['id']);
            $purchaseOrderNumber = $wooOrderId;
            // $purchaseOrderNumber = 'TEST-' . $purchaseOrderNumber;
            echo date('c ') . $wooOrderId . ' ' . $purchaseOrderNumber. PHP_EOL;
            /**
             * @var OrderMapper $orderMapper
             */
            $orderMapper = Yii::$container->get($orderMapperClass, [$neto, $wooOrder]);
            if (!isset($netoOrders[$purchaseOrderNumber])) {                
                $orderData = $orderMapper->orderData();
                if (empty($orderData)) {
                    echo date('c ') . $wooOrderId . ' mapped order data is empty' . PHP_EOL;
                } else {
                    $netoOrderResult = $this->createNetoOrder($orderMapper);
                    if (!empty($netoOrderResult['Order']['OrderID'])) {
                        $this->createNetoPayment($orderMapper, $netoOrderResult['Order']['OrderID']);
                    }                
                }
            } elseif (false && !empty($orderMapper->isPaid()) && empty(ArrayHelper::getValue($netoOrders, [$purchaseOrderNumber, 'OrderPayment', 0]))) {
                $this->createNetoPayment($orderMapper, ArrayHelper::getValue($netoOrders, [$purchaseOrderNumber, 'OrderID']));
            } elseif (ArrayHelper::getValue($netoOrders, [$purchaseOrderNumber, 'OrderStatus']) == 'Dispatched') {                
                $this->syncWooShipment($orderMapper, $netoOrders[$purchaseOrderNumber]);
            }
        }
    }

    /**
     * @param OrderMapper $orderMapper
     */
    protected function createNetoOrder($orderMapper)
    {
        $orderData = $orderMapper->orderData();
        if (empty($orderData)) {
            return;
        }
        // $orderData['OrderStatus'] = 'On Hold';
        $data = [
            'Order' => [$orderData],
        ];
        $orderResult = null;
        $neto =  $this->getNeto();
        try {
            echo date('c ') . json_encode(['order data' => $data]) . PHP_EOL;
            $orderResult = $neto->sendRequest('AddOrder', 'post', Json::encode($data));
            echo date('c ') . json_encode(['orderResult' => $orderResult]) . PHP_EOL;
        } catch (\Exception $e) {
            echo date('c ') . 'Neto Order Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
        }
        return $orderResult;
    }

    /**
     * @param OrderMapper $orderMapper
     * @param string $netoOrderId
     */
    protected function createNetoPayment($orderMapper, $netoOrderId)
    {
        $neto = $this->getNeto();
        $netoOrderResult = $neto->sendRequest('GetOrder', 'post', Json::encode([
            'Filter' => [
                'OrderID' => $netoOrderId,
                'OutputSelector' => ['GrandTotal']
            ]
        ]));
        $netoOrder = ArrayHelper::getValue($netoOrderResult, ['Order', 0]);        
        $paymentAmount = ArrayHelper::getValue($netoOrder, ['GrandTotal']);
        $paymentDate = ArrayHelper::getValue($orderMapper->getWooOrder(), ['date_paid']);        
        if (!empty($paymentAmount) && !empty($paymentDate)) {
            $paymentDate = Carbon::parse($paymentDate)->format('Y-m-d');        
            $wooOrderId = ArrayHelper::getValue($orderMapper->getWooOrder(), ['id']);            
            $paymentsData = [
                'Payment' => [
                    [
                        'OrderID' => $netoOrderId,
                        'PaymentMethodName' => $orderMapper->paymentMethodName(),
                        'AmountPaid' => number_format($paymentAmount, 2, '.', ''),
                        'DatePaid' => $paymentDate,
                        'CardAuthorisation' => $this->newGuid(),
                    ]
                ]
            ];
            try {
                echo date('c ') . $wooOrderId . ' ' . json_encode(['paymentsData' => $paymentsData]) . PHP_EOL;
                $paymentResult = $neto->sendRequest('AddPayment', 'post', Json::encode($paymentsData));
                echo date('c ') . $wooOrderId . ' ' . json_encode(['orderResult' => $paymentResult]) . PHP_EOL;
            } catch (\Exception $e) {
                echo date('c ') . $wooOrderId . ' Neto Payment Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
            }        
        }        
    }

    /**
     * @param OrderMapper $orderMapper
     * @param array $netoOrder
     */
    protected function syncWooShipment($orderMapper, $netoOrder)
    {
        $wooOrder = $orderMapper->getWooOrder();
        $wooOrderId = ArrayHelper::getValue($wooOrder, ['id']);
        $woo = $this->getWoo();
        $shipments = [];
        $shipmentNumber = null;
        $shipmentsTrackingId = [];
        $netoOrderLineShippingTracking = null;
        try {
            $shipments = $woo->sendApiRequest('v3/orders/' . $wooOrderId . '/shipment-trackings/');
            echo date('c ') . $wooOrderId . ' ' . json_encode(['shipments' => $shipments]) . PHP_EOL;
            $shipmentNumber = ArrayHelper::getValue($shipments, [0, 'tracking_number']);
            $shipmentsTrackingId = ArrayHelper::getColumn($shipments, 'tracking_id');
        } catch (\Exception $e) {
            echo date('c ') . $wooOrderId . ' Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;            
        }        
        $netoOrderLines = ArrayHelper::getValue($netoOrder, 'OrderLine', []) ?? [];
        $shipmentData = [];
        foreach ($netoOrderLines as $netoOrderLine) {
            $netoOrderLineShippingMethod = ArrayHelper::getValue($netoOrderLine, 'ShippingMethod');
            $netoOrderLineShippingTracking = ArrayHelper::getValue($netoOrderLine, 'ShippingTracking');
            if (empty($netoOrderLineShippingTracking)) {
                $netoOrderLineShippingTracking = 'NotTracked';
            }
            if (!empty($netoOrderLineShippingMethod)) {
                $shipmentData = [
                    'tracking_number' => $netoOrderLineShippingTracking,
                    'tracking_provider' => $orderMapper->carrierName($netoOrderLineShippingMethod),
                ];                    
                if (empty($shipmentData['tracking_provider'])) {
                    unset($shipmentData['tracking_provider']);
                    $shipmentData['custom_tracking_provider'] = $netoOrderLineShippingMethod;
                }
                break;
            }
        }
        if (!empty($shipmentData) && $netoOrderLineShippingTracking !== $shipmentNumber) {
            try {
                foreach ($shipmentsTrackingId as $shipmentTrackingId) {
                    echo date('c ') . $wooOrderId . ' DELETE ' . json_encode(['shipmentTrackingId' => $shipmentTrackingId]) . PHP_EOL;
                    $shipmentTracking = $woo->sendApiRequest('v3/orders/' . $wooOrderId . '/shipment-trackings/' . $shipmentTrackingId, 'delete');
                    echo date('c ') . $wooOrderId . ' DELETE ' . json_encode(['shipmentTracking' => $shipmentTracking]) . PHP_EOL;
                }
                echo date('c ') . $wooOrderId . ' ' . json_encode(['shipmentData' => $shipmentData]) . PHP_EOL;
                $shipments = $woo->sendApiRequest('v3/orders/' . $wooOrderId . '/shipment-trackings', 'post', Json::encode($shipmentData));
                echo date('c ') . $wooOrderId . ' ' . json_encode(['shippingResult' => $shipments]) . PHP_EOL;
            } catch (\Exception $e) {
                echo date('c ') . $wooOrderId . ' Shipment Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        }
        if (!empty($shipments) && ArrayHelper::getValue($wooOrder, ['status']) == 'processing') {
            try {
                $orderData = ['status' => 'completed'];
                echo date('c ') . $wooOrderId . ' ' . json_encode(['orderData' => $orderData]) . PHP_EOL;                        
                $orderResult = $woo->sendApiRequest('v3/orders/' . $wooOrderId, 'post', Json::encode($orderData));
                echo date('c ') . $wooOrderId . ' ' . json_encode(['orderResult' => $orderResult]) . PHP_EOL;
            } catch (\Exception $e) {
                echo date('c ') . $wooOrderId . ' Order update Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        }
        return $shipments;
    }

    private  function newGuid()
    {
        $guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        return strtolower($guid);
    }

    /**
     * Return neto order placed since
     * @param string|array $ordersId Orders Id
     * @param string $externalRef External Ref
     * @param Carbon|null $placedSince reference
     * @param array $orderStatus order status
     * @return array
     */
    private function getNetoOrders($ordersId = null, $externalRef = null, $placedSince = null, $orderStatus = null)
    {
        $outputSelectors = [
            'Username',
            'Email',
            'BillAddress',
            'ShipAddress',
            'DeliveryInstruction',
            'DatePaid',
            'DateInvoiced',
            'DatePlaced',
            'DateCompleted',
            'OrderStatus',
            'PurchaseOrderNumber',
            'OrderLine',
            'OrderLine.Extra',
            'OrderLine.ExtraOptions',
            'OrderLine.PickQuantity',
            'OrderLine.Quantity',
            'OrderLine.ProductName',
            'OrderLine.UnitPrice',
            'OrderLine.ShippingMethod',
            'OrderLine.ShippingTracking',
            'OrderLine.QuantityShipped',
            'ShippingSignature',
            'InternalOrderNotes',
            'OrderLine.ExternalSystemIdentifier',
            'OrderLine.ExternalOrderReference',
            'OrderLine.ExternalOrderLineReference',
            'SalesChannel',
            'OrderPayment',
        ];
        if (empty($orderStatus)) {
            $orderStatus = [
                'New',
                'New Backorder',
                'Backorder Approved',
                'Pick',
                'Pack',
                'Pending Pickup',
                'Pending Dispatch',
                'Dispatched',
                'Uncommitted',
                'On Hold',
            ];
        }
        if (count($orderStatus) == 1) {
            $orderStatus = reset($orderStatus);
        }
        $orderMapperClass = $this->getOrderMapperClass();
        $filter = [
            'SalesChannel' => $orderMapperClass::salesChannel(),
            'OrderStatus' => $orderStatus,
            'OutputSelector' => $outputSelectors
        ];
        if (!empty($ordersId)) {
            $filter['OrderID'] = !is_array($ordersId) ? explode(',', $ordersId) : $ordersId;
        } elseif (!empty($externalRef)) {
            $filter['ExternalOrderReference'] = $externalRef;
        } elseif (empty($placedSince)) {
            $placedSince = Carbon::today();
        }
        if (!empty($placedSince)) {
            $filter['DatePlacedFrom'] = $placedSince->startOfDay()->format('Y-m-d H:i:s');
        }        
        $data = [
            'Filter' => $filter
        ];
        // var_dump($data);
        $neto = $this->getNeto();
        $result = $neto->sendRequest('GetOrder', 'post', Json::encode($data));
        return ArrayHelper::getValue($result, 'Order') ?? [];
    }

    public function actionProduct($startSku = null, $syncLimit = null)
    {
        $wooProducts = [];
        $wooProductsCount = 0;
        $netoProductsCount = 0;
        $syncProductsCount = 0;
        $page = 1;
        $woo = $this->getWoo();
        do {
            $wooProductsChunk = $woo->sendApiRequest(['v3/products', 'page' => $page, 'per_page' => 100]);
            if (!empty($wooProductsChunk)) {
                $wooProductsCount += count($wooProductsChunk);
                $wooProductsChunk = ArrayHelper::map($wooProductsChunk, 'sku', 'id');
                $wooProducts = array_merge($wooProducts, $wooProductsChunk);
                echo date('c ') . 'Total Woo Product: ' . $page . '/' . $wooProductsCount . PHP_EOL;
            }
            $page++;
        } while (!empty($wooProductsChunk));        
        $page = 0;
        $nonVariantProducts = [];
        if (!empty($startSku) && $syncLimit == 1) {
            $filter = ['SKU' => $startSku];
        } else {
            $filter = [];
        }
        do {
            //$netoProductsChunk = $this->getNetoProducts(['ParentSKU'], [], 10000, $page);
            $netoProductsChunk = $this->getNetoProducts(['ParentSKU'], $filter, 10000, $page);
            if (!empty($netoProductsChunk)) {
                $netoProductsChunk = array_filter($netoProductsChunk, function ($item) {
                    return empty($item['ParentSKU']);
                });
            }
            $nonVariantProducts = array_merge($nonVariantProducts, $netoProductsChunk);
            $page++;
        } while (!empty($netoProductsChunk));
        $nonVariantProducts = ArrayHelper::getColumn($nonVariantProducts, 'SKU');
        echo date('c ') . 'Total Non variant Neto Product: ' . count($nonVariantProducts) . PHP_EOL;        
        $productMapperClass = $this->getProductMapperClass();
        $outputSelector = $productMapperClass::netoAttributes();
        $page = 0;
        do {
            $netoProductsChunk = $this->getNetoProducts($outputSelector, ['SKU' => $nonVariantProducts], 10000, $page);
            if (!empty($netoProductsChunk)) {
                $netoProductsCount += count($netoProductsChunk);
                foreach ($netoProductsChunk as $netoProduct) {
                    $sku = ArrayHelper::getValue($netoProduct, ['SKU']);
                    if (!empty($startSku)) {
                        if ($startSku == $sku) {
                            $startSku = null;
                        } else {
                            continue;
                        }
                    }
                    $productMapper = $this->getProductMapper($netoProduct, ArrayHelper::getValue($wooProducts, [$sku]));
                    if ($productMapper->type() == 'variation') {
                        continue;
                    }
                    echo date('c ') . 'Sync: ' . $sku . ' | ' . $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
                    $this->syncProduct($productMapper);
                    $syncProductsCount++;
                    if (!empty($syncLimit) && $syncProductsCount >= $syncLimit) {
                        break 2;
                    }
                }
                echo date('c ') . 'Updated: ' . $page . '/' .  $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
            }
            $page++;
        } while (!empty($netoProductsChunk));
        echo date('c ') . 'Synced Product: ' . $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
    }

    /**
     * @param ProductMapper $productMapper
     * @return array
     */
    private function syncProduct($productMapper)
    {
        $sku = $productMapper->sku();
        $productData = $productMapper->productData();
        $result = null;
        if (empty($productData)) return $result;
        try {
            $woo = $this->getWoo();
            // echo date('c ') . $sku . ' ' . json_encode(['wooProductId' => $productMapper->wooProductId(), 'productData' => $productData]) . PHP_EOL;
            if ($productMapper->isUpdate()) {
                $result = $woo->sendApiRequest('v3/products/' . $productMapper->wooProductId() . '/', 'put', Json::encode($productData));
            } else {
                $result = $woo->sendApiRequest('v3/products/', 'post', Json::encode($productData));
            }            
            // echo date('c ') . $sku . ' ' . json_encode(['product' => $result]) . PHP_EOL;
            $productMapper->wooProductId(ArrayHelper::getValue($result, ['id']));
            if ($productMapper->type() == 'variable') {
                $this->syncProductVariation($productMapper);
            }
        } catch (\Exception $e) {
            echo date('c ') . $sku . ' Product Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
        }
        return $result;
    }

    /**
     * @param ProductMapper $productMapper
     * @return array
     */
    private function syncProductVariation($productMapper)
    {
        $wooProductId = $productMapper->wooProductId();
        $sku = $productMapper->sku();
        $productVariation = $productMapper->productVariations();
        $result = null;
        if (empty($wooProductId) || $productMapper->type() !== 'variable' || empty($productVariation)) return $result;
        try {
            $woo = $this->getWoo();
            // echo date('c ') . $sku . ' ' . json_encode(['wooProductId' => $productMapper->wooProductId(), 'productVariation' => $productVariation]) . PHP_EOL;
            $result = $woo->sendApiRequest('v3/products/' . $wooProductId . '/variations/batch', 'post', Json::encode($productVariation));
            // echo date('c ') . $sku . ' ' . json_encode(['variations' => $result]) . PHP_EOL;
        } catch (\Exception $e) {
            echo date('c ') . $sku . ' Variations Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
        }
        return $result;
    }

    public function actionInventory($startSku = null, $syncLimit = null)
    {
        $startingTimestamp = time();
        $netoProductsCount = 0;
        $syncProductsCount = 0;
        $wooProductsSku = [];
        $page = 1;
        $woo = $this->getWoo();
        do {
            $wooProductsChunk = $woo->sendApiRequest(['v3/products', 'page' => $page, 'per_page' => 100]);
            foreach ($wooProductsChunk as $wooProduct) {
                $sku = ArrayHelper::getValue($wooProduct, ['sku']);
                $productId = ArrayHelper::getValue($wooProduct, ['id']);
                $wooProductsSku[$sku] = $productId;
                $variations = ArrayHelper::getValue($wooProduct, ['variations']);
                if (!empty($variations)) {
                    $productsVariation = $woo->sendApiRequest(['v3/products/' . $productId . '/variations', 'per_page' => 100]);
                    $productsVariationSku = ArrayHelper::map($productsVariation, 'sku', 'id');
                    $wooProductsSku = array_merge($wooProductsSku, $productsVariationSku);
                    // echo date('c ') . 'Total Woo Product Variations: ' . $sku . ' ' . count($productsVariation) . ' / ' . count($wooProductsSku). PHP_EOL;
                }
            }            
            echo date('c ') . 'Total Woo Product: ' . $page . '/' . count($wooProductsChunk) . '/' . count($wooProductsSku). PHP_EOL;
            $page++;
        } while (!empty($wooProductsChunk));
        $productMapperClass = $this->getProductMapperClass();
        $outputSelector = $productMapperClass::netoAttributes();
        $page = 0;
        $syncedProducts = [];
        do {
            $netoProductsChunk = $this->getNetoProducts($outputSelector, [], 10000, $page);
            $netoProductsChunk = ArrayHelper::index($netoProductsChunk, 'SKU');
            if (!empty($netoProductsChunk)) {
                $netoProductsCount += count($netoProductsChunk);
                foreach ($netoProductsChunk as $sku => $netoProduct) {
                    if (!empty($startSku)) {
                        if ($startSku == $sku) {
                            $startSku = null;
                        } else {
                            continue;
                        }
                    }
                    $parentSku = ArrayHelper::getValue($netoProduct, ['ParentSKU']);
                    $productMapper = $this->getProductMapper($netoProduct, ArrayHelper::getValue($wooProductsSku, [$sku]));                    
                    echo date('c ') . 'Sync: ' . $sku . (!empty($parentSku) ? '=>' . $parentSku : '') . ' ' . (!isset($wooProductsSku[$sku]) ? 'create ' : '') . $productMapper->type() . ' | ' . $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
                    if (isset($wooProductsSku[$sku])) {
                        $this->syncInventory($productMapper);
                    } elseif ($productMapper->type() == 'variation') {
                        if (!in_array($parentSku, $syncedProducts)) {
                            $netoParentProduct = ArrayHelper::getValue($netoProductsChunk, [$parentSku], ArrayHelper::getValue($this->getNetoProducts($outputSelector, ['SKU' => $parentSku], 1), [0]));
                            if (empty($netoParentProduct)) {
                                continue;
                            }
                            $parentProductMapper = $this->getProductMapper($netoParentProduct, ArrayHelper::getValue($wooProductsSku, [$parentSku]));
                            $this->syncProduct($parentProductMapper);
                            $syncedProducts[] = $parentSku;
                        }
                    } else {
                        $this->syncProduct($productMapper);
                        $syncedProducts[] = $sku;
                    }                    
                    $syncProductsCount++;
                    if (!empty($syncLimit) && $syncProductsCount >= $syncLimit) {
                        break 2;
                    }
                }
                echo date('c ') . 'Updated: ' . $page . '/' .  $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
            }
            $page++;
        } while (!empty($netoProductsChunk));
        echo date('c ') . 'Synced Inventory: ' . $syncProductsCount . '/' . $netoProductsCount . PHP_EOL;
        echo date('c ') . 'Done in '  . Yii::$app->formatter->asDuration(time() - $startingTimestamp) . PHP_EOL;
    }

    /**
     * @param ProductMapper $productMapper
     * @return array
     */
    private function syncInventory($productMapper)
    {
        $sku = $productMapper->sku();
        $inventoryData = $productMapper->inventoryData();
        $result = null;
        if (empty($inventoryData)) return $result;
        try {
            $woo = $this->getWoo();
            // echo date('c ') . $sku . ' ' . json_encode(['wooProductId' => $productMapper->wooProductId(), 'inventoryData' => $inventoryData]) . PHP_EOL;
            if ($productMapper->isUpdate()) {
                $result = $woo->sendApiRequest('v3/products/' . $productMapper->wooProductId() . '/', 'put', Json::encode($inventoryData));
            }            
            // echo date('c ') . $sku . ' ' . json_encode(['product' => $result]) . PHP_EOL;
        } catch (\Exception $e) {
            echo date('c ') . $sku . ' Product Error: ' . PHP_EOL . $e->getMessage() . PHP_EOL;
        }
        return $result;
    }

    public function getNetoProducts($outputSelector = [], $filter = [], $limit = 0, $page = 0, $isActive = true, $approved = true)
    {
        $data = [
            'Filter' => array_merge($filter, [
                'Page' => $page,
                'Limit' => $limit,
                'OrderBy' => 'DateAdded',
                // 'Approved' => $approved,
                'IsActive' => $isActive,                
                'OutputSelector' => $outputSelector
            ])
        ];
        $neto = $this->getNeto();
        $result = $neto->sendRequest('GetItem', 'post', Json::encode($data));
        return ArrayHelper::getValue($result, 'Item');
    }

    public function actionCheck()
    {   
        /**
         * @var Neto $neto
         */
        $neto = $this->getNeto();

        /**
         * @var Woocommerce $woo
         */
        $woo = $this->getWoo();
        
        // echo json_encode($this->getNetoOrders(null, null, Carbon::today()->subMonth())) . PHP_EOL;
        // echo json_encode($this->getNetoOrders('NT2149')) . PHP_EOL;
        // echo json_encode($this->getNetoOrders(null, '25405')) . PHP_EOL;
        // echo json_encode($neto->sendRequest('GetPaymentMethods', 'post', '{}')) . PHP_EOL;
        // echo json_encode($neto->sendRequest('GetShippingMethods', 'post', '{}')) . PHP_EOL;
        // echo json_encode($neto->getProducts(Neto::PRODUCT_ELEMENTS, 'C470377', 0, 0, true, null)) . PHP_EOL;
        // echo json_encode($woo->getOrders(null, null, null, ['processing', 'on-hold'])) . PHP_EOL;
        // echo json_encode($woo->getOrdersById(25413)) . PHP_EOL;
        // echo json_encode ($this->getNetoProducts(ProductMapper::netoAttributes(), ['SKU' => 'PC470377'], 10000)) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest('v3/shipping_methods')) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest('v3/payment_gateways')) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest(['v3/products/categories', 'per_page' => 100])) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest(['v2/products/brands', 'per_page' => 100])) . PHP_EOL;
        echo json_encode($woo->sendApiRequest('v3/products/26583')) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest('v3/products/26583/variations/26599')) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest(['v3/products', 'sku' => 'PC413309'])) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest(['v3/products/25712'], 'put', json_encode(['product_brand' => [1630]]))) . PHP_EOL;
        // echo json_encode($woo->sendApiRequest(['v3/products', 'sku' => 'PC470377'])) . PHP_EOL;
        // echo json_encode($woo->sendWpRequest(['v2/product_brand', 'per_page' => 100])) . PHP_EOL;
        // echo json_encode($woo->sendWpRequest(['v2/taxonomies', 'type' => 'product'])) . PHP_EOL;
    }

    public function actionData()
    {
        // $woo = $this->getWoo();
        return 'a';
    }
}
