<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Woo2Controller extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $host = 'https://onesixeightlondon.com.au/wp-json/wc/v3/coupons/:id?context=view&context=view';
        $username = 'ck_591e90f2dc22019bd1745865d634406e264f7372';
        $password = 'cs_543c3da23611162df799a813ec9c63be01e2763c';

        // $additionalHeaders = '';
        // $ch = curl_init($host);
        // // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', $additionalHeaders));
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        // curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        // curl_setopt($ch, CURLOPT_HTTPGET, 1);
        // // curl_setopt($ch, CURLOPT_POST, 1);
        // // curl_setopt($ch, CURLOPT_POSTFIELDS, $payloadName);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // $return = curl_exec($ch);
        // curl_close($ch);

        // print_r($return);

        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, "https://onesixeightlondon.com.au/wp-json/wc/coupons/:id?context=view&context=view");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

        // grab URL and pass it to the browser
        curl_exec($ch);

        // close cURL resource, and free up system resources
        curl_close($ch);
    }
}
