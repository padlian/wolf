<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Json;
use yii\mutex\FileMutex;

/**
 * Class BaseMutexController
 * @package app\commands
 */
class BaseMutexController extends Controller
{
    /**
     * @var string $config
     */
    public $config;

    /**
     * @var string $mutex
     */
    public $mutex = true;

    /**
     * @var boolean $force
     */
    public $force;
    /**
     * @var boolean $overwrite
     */
    public $overwrite;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return ['config', 'mutex', 'force', 'overwrite'];
    }
    
    /**
     * @inheritdoc
     */
    public function optionAliases()
    {
        return ['c' => 'config', 'm' => 'mutex', 'f' => 'force', 'ow' => 'overwrite'];
    }

    /**
     * {@inheritDoc}
     * @see \yii\base\Controller::beforeAction()
     */
    public function beforeAction($action)
    {
        /**
         * @var FileMutex $mutex
         */
        $mutex = $this->module->mutex;
        $mutexName = Json::encode([$this->route, $this->getOptionValues($action->id)]);
        if (empty($this->force) && $this->mutex && !$mutex->acquire($mutexName, 3)) {
            $this->stdout('Another process of ' . $mutexName . ' is running!' . PHP_EOL);
            return false;
        }
        return parent::beforeAction($action);
    }
}
