<?php

namespace app\commands;

use app\components\Activecampaign;
use app\components\Linnworks;
use app\components\Marketplacer;
use app\components\Mirakl;
use app\components\Mysalemarketplace;
use app\components\Neto;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\Unleashed;
use app\components\Woocommerce;
use app\forms\BaseProductMapper;
use app\forms\BaseOrderMapper;
use InvalidArgumentException;
use yii\swiftmailer\Mailer;

/**
 * BaseSync Controller
 *
 * @author Yoyon Cahyono <yoyoncahyono@gmail.com>
 */
class BaseSyncController extends BaseMutexController
{
    /**
     * @return array|string
     */
    protected function getConfigs()
    {
        $configs = [];
        return $configs;
    }

    /**
     * @return array|string
     */
    protected function getConfig($componentName)
    {
        if (empty($this->config)) throw new InvalidArgumentException('config option must be set');
        $configs = $this->getConfigs();
        $config = ArrayHelper::getValue($configs, [$this->config, $componentName]);
        if (empty($config)) {
            throw new InvalidArgumentException('Config for "' . $this->config . '/' . $componentName . '" not found');
        }
        return $config;
    }

    /**
     * @return Neto
     */
    protected function getNeto()
    {
        $netoConfig = $this->getConfig('neto');
        /**
         * @var Neto $neto
         */
        $neto = Yii::$container->get(Neto::class, [], $netoConfig);
        return $neto;
    }

    /**
     * @return Mirakl
     */
    protected function getMirakl()
    {
        $unleashedConfig = $this->getConfig('mirakl');
        /**
         * @var Mirakl $mirakl
         */
        $mirakl = Yii::$container->get(Mirakl::class, [], $unleashedConfig);
        return $mirakl;
    }

    /**
     * @return Unleashed
     */
    protected function getUnleashed()
    {
        $unleashedConfig = $this->getConfig('unleashed');
        /**
         * @var Unleashed $unleashed
         */
        $unleashed = Yii::$container->get(Unleashed::class, [], $unleashedConfig);
        return $unleashed;
    }

    /**
     * @return Mysalemarketplace
     */
    protected function getMysalemarketplace()
    {
        $msmpConfig = $this->getConfig('mysalemarketplace');
        /**
         * @var Mysalemarketplace $msmp
         */
        $msmp = Yii::$container->get(Mysalemarketplace::class, [], $msmpConfig);
        return $msmp;
    }

    /**
     * @return Woocommerce
     */
    protected function getWoocommerce()
    {
        $wooConfig = $this->getConfig('woo');
        /**
         * @var Woocommerce $woo
         */
        $woo = Yii::$container->get(Woocommerce::class, [], $wooConfig);
        return $woo;
    }

    /**
     * @return Linnworks
     */
    protected function getLinnworks()
    {
        $linConfig = $this->getConfig('linnworks');
        /**
         * @var Linnworks $lin
         */
        $lin = Yii::$container->get(Linnworks::class, [], $linConfig);
        return $lin;
    }

    /**
     * @return Activecampaign
     */
    protected function getActivecampaign()
    {
        $acConfig = $this->getConfig('activecampaign');
        /**
         * @var Activecampaign $ac
         */
        $ac = Yii::$container->get(Activecampaign::class, [], $acConfig);
        return $ac;
    }

    /**
     * @return Marketplacer
     */
    protected function getMarketplacer()
    {
        $config = $this->getConfig('marketplacer');
        /**
         * @var Marketplacer $marketplacer
         */
        $marketplacer = Yii::$container->get(Marketplacer::class, [], $config);
        return $marketplacer;
    }
    
    /**
     * @return string
     */
    protected function getOrderMapperClass()
    {
        $orderMapperClass = $this->getConfig('ordermapper');
        if (empty($orderMapperClass) || !class_exists($orderMapperClass)) {
            $orderMapperClass = BaseOrderMapper::class;
        }
        return $orderMapperClass;
    }

    /**
     * @param array $params
     * @return BaseOrderMapper
     */
    protected function getOrderMapper($params)
    {
        $orderMapperClass = $this->getOrderMapperClass();
        $orderMapper = Yii::$container->get($orderMapperClass, $params);
        return $orderMapper;        
    }

    /**
     * @return string
     */
    protected function getProductMapperClass()
    {
        $productMapperClass = $this->getConfig('productmapper');
        if (empty($productMapperClass) || !class_exists($productMapperClass)) {
            $productMapperClass = BaseProductMapper::class;
        }
        return $productMapperClass;
    }

    /**
     * @param array $netoProduct
     * @return BaseProductMapper
     */
    protected function getProductMapper($params)
    {
        $productMapperClass = $this->getProductMapperClass();
        $productMapper = Yii::$container->get($productMapperClass, $params);
        return $productMapper;        
    }

    /**
     * @return Mailer
     */
    protected function getMailer()
    {
        $mailerConfig = $this->getConfig('mailer');
        /**
         * @var Mailer $mailer
         */
        $mailer = Yii::$container->get(Mailer::class, [], $mailerConfig);
        return $mailer;
    }

    protected function newGuid()
    {
      $guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
      return strtolower($guid);
    }
}
