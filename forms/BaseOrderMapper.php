<?php

namespace app\modules\wolfgroup\forms;

use app\components\Neto;
use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * BaseOrderMapper base order mapper
 */
class BaseOrderMapper extends BaseObject
{
    /**
     * @var array
     */
    protected $sourceOrder;

    /**
     * @inheritdoc
     * @param array $sourceOrder
     * @param array $config
     */
    public function __construct($sourceOrder, $config = array())
    {
        $this->sourceOrder = $sourceOrder;
        parent::__construct($config);
    }

    /**
     * Return source order
     * @return array
     */
    public function getSourceOrder()
    {
        return $this->sourceOrder;
    }

    /**
     * Return mapped order data
     * @return array
     */
    public function orderData()
    {
        $sourceOrder = $this->sourceOrder;
        return $sourceOrder;
    }
}
