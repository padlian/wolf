<?php

namespace app\modules\wolfgroup\forms;

use yii\base\BaseObject;

/**
 * BaseProductMapper base product mapper
 */
class BaseProductMapper extends BaseObject
{
    /**
     * @var array
     */
    protected $sourceProduct;

    /**
     * @inheritdoc
     * @param array $sourceProduct
     * @param array $config
     */
    public function __construct($sourceProduct, $config = array())
    {
        $this->sourceProduct = $sourceProduct;
        parent::__construct($config);
    }

    /**
     * Return source 
     * @return array
     */
    public function getSourceProduct()
    {
        return $this->sourceProduct;
    }

    /**
     * Return mapped product data
     * @return array
     */
    public function productData()
    {
        $sourceProduct = $this->sourceProduct;
        return $sourceProduct;
    }
}
