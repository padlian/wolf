<?php

namespace app\forms;


use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


class WooData extends BaseObject
{
    // public $id;
    /**
     * @var array
     */
    protected $wooOrder;

    protected $maropostOrder;

    /**
     * @inheritdoc
     * @param Neto $neto
     * @param array $wooOrder woocommerce order
     */
    public function __construct($wooOrder, $config = array())
    {
        $this->wooOrder = $wooOrder;
        // $this->id = $wooOrder[0]->id;
        // parent::__construct($config);
    }

    public function getWooOrder()
    {
        return $this->wooOrder;
    }

    public function maropostFormat($id = 0)
    {
        $maropostData = [
            // 'id' => $this->wooOrder[0]->id,
            'Username' => $this->wooOrder[$id]->billing->email,
            'EmailAddress' => $this->wooOrder[$id]->billing->email,
            'UserGroup' => '',
            'BillFirstName' => $this->wooOrder[$id]->billing->first_name,
            'BillLastName' => $this->wooOrder[$id]->billing->last_name,
            'BillCompany' => $this->wooOrder[$id]->billing->company,
            'BillStreet1' => $this->wooOrder[$id]->billing->address_1,
            'BillStreet2' => $this->wooOrder[$id]->billing->address_2,
            'BillCity' => $this->wooOrder[$id]->billing->city,
            'BillState' => $this->wooOrder[$id]->billing->state,
            'BillPostCode' => $this->wooOrder[$id]->billing->postcode,
            'BillPhone' => $this->wooOrder[$id]->billing->phone,
            'ShipFirstName' => $this->wooOrder[$id]->shipping->first_name,
            'ShipLastName' => $this->wooOrder[$id]->shipping->last_name,
            'ShipCompany' => $this->wooOrder[$id]->shipping->company,
            'ShipStreet1' => $this->wooOrder[$id]->shipping->address_1,
            'ShipStreet2' => $this->wooOrder[$id]->shipping->address_2,
            'ShipCity' => $this->wooOrder[$id]->shipping->city,
            'ShipState' => $this->wooOrder[$id]->shipping->state,
            'ShipPostCode' => $this->wooOrder[$id]->shipping->postcode,
            'ShipPhone' => $this->wooOrder[$id]->shipping->country,
        ];
        $this->maropostOrder = $maropostData;
        // return $this->wooOrder[0]->id;
        return $this->maropostOrder;
    }
}