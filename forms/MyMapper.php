<?php

namespace app\forms\woocommerce;

use app\components\Neto;
use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * OrderMapper woocommerce to neto order mapper
 */

class MyMapper extends BaseObject
{
    /**
     * @var array
     */
    protected $wooOrder;

    /**
     * @inheritdoc
     * @param array $wooOrder woocommerce order
     */
    public function __construct($wooOrder, $config = array())
    {
        $this->wooOrder = $wooOrder;
        parent::__construct($config);
    }

    public function getWooOrder()
    {
        return $this->wooOrder;
    }

    
}