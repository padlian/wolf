<?php

namespace app\modules\wolfgroup\forms\woocommerce;

use app\components\Neto;
use app\components\Woocommerce;
use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Exception;

/**
 * ProductMapper neto to woocommerce product mapper
 */
class ProductMapper extends BaseObject
{
    /**
     * @var array
     */
    protected $netoProduct;
    /**
     * @var array
     */
    protected $parentProduct;
    /**
     * @var integer
     */
    protected $wooProductId;
    /**
     * @var Woocommerce
     */
    protected $woo;
    /**
     * @var Neto
     */
    protected $neto;

    /**
     * @var array
     */
    protected static $wooCategories;

    /**
     * @var array
     */
    protected static $netoCategories;

    /**
     * @var array
     */
    protected static $wooAttributes;

    /**
     * @var array
     */
    protected static $wooBrands;

    /**
     * @var array
     */
    protected $childProducts;

    /**
     * @var array
     */
    protected $upsellProducts;

    /**
     * @var array
     */
    protected $wooVariations;

    public static function netoAttributes()
    {
        return [
            'ParentSKU', 'UpsellProducts',
            'Name', 'Description', 'ShortDescription', 'AvailableSellQuantity', 'ShippingLength', 'ShippingWidth', 
            'ShippingHeight', 'ShippingWeight', 'RelatedContents', 'Categories', 'Approved', 'IsActive', 'PriceGroups', 'RRP', 'DefaultPrice',
            'PromotionStartDate', 'PromotionExpiryDate', 'Features', 'Brand',
            'Images', 'SEOPageTitle', 'SEOMetaKeywords', 'SEOMetaDescription', 'ItemSpecifics'
        ];
    }

    /**
     * @inheritdoc
     * @param array $netoProduct
     * @param array $parentProduct
     * @param integer $wooProductId
     * @param Woocommerce $woo
     * @param Neto $neto
     */
    public function __construct($netoProduct, $parentProduct, $wooProductId, $woo, $neto, $config = array())
    {
        $this->netoProduct = $netoProduct;
        $this->parentProduct = $parentProduct;
        $this->wooProductId = $wooProductId;
        $this->woo = $woo;
        $this->neto = $neto;
        parent::__construct($config);
    }

    /**
     * Return woo product id
     * @return integer
     */
    public function wooProductId($wooProductId = null)
    {
        if (is_integer($wooProductId)) {
            $this->wooProductId = $wooProductId;
        }
        return $this->wooProductId;
    }

    /**
     * Return list of woo categories
     * @return array
     */
    public function wooCategories()
    {
        if (is_null(static::$wooCategories)) {
            $page = 1;
            do {
                $wooCategoriesChunk = $this->woo->sendApiRequest(['v3/products/categories', 'per_page' => 100, 'page' => $page]);
                if (!empty($wooCategoriesChunk)) {
                    $wooCategories = ArrayHelper::map($wooCategoriesChunk, function ($item) {
                        return html_entity_decode(strtolower($item['name']));
                    }, 'id');
                    static::$wooCategories = array_merge(static::$wooCategories ?? [], $wooCategories);
                }                
                $page++;
            } while (!empty($wooCategoriesChunk));                        
        }
        return static::$wooCategories;
    }

    /**
     * Return list of neto categories
     * @return array
     */
    public function netoCategories()
    {
        if (is_null(static::$netoCategories)) {
            $page = 0;
            $limit = 1000;
            do {
                $data = [
                    'Filter' => [
                        // 'Active' => true,
                        'Page' => $page,
                        'Limit' => 1000,
                        'OutputSelector' => [
                            'CategoryID',
                            'ParentCategoryID',
                            'CategoryName',
                        ]
                    ]
                ];
                $netoCategoriesResult = $this->neto->sendRequest('GetCategory', 'post', Json::encode($data));
                $netoCategoriesChunk = ArrayHelper::getValue($netoCategoriesResult, ['Category']);
                if (!empty($netoCategoriesChunk)) {
                    static::$netoCategories = array_merge(static::$netoCategories ?? [], $netoCategoriesChunk);
                }
                $page++;
            } while (!empty($netoCategoriesChunk));
            if (!empty(static::$netoCategories)) {
                static::$netoCategories = ArrayHelper::index(static::$netoCategories, 'CategoryID');
            }
        }
        return static::$netoCategories;
    }

    /**
     * Return list of woo brands
     * @return array
     */
    public function wooBrands()
    {
        if (is_null(static::$wooBrands)) {
            try {
                $page = 1;
                do {
                    $wooBrandsChunk = $this->woo->sendApiRequest(['v2/products/brands', 'per_page' => 100, 'page' => $page]);
                    if (!empty($wooBrandsChunk)) {
                        $wooBrands = ArrayHelper::map($wooBrandsChunk, function ($item) {
                            return html_entity_decode(strtolower($item['name']));
                        }, 'id');
                        static::$wooBrands = array_merge(static::$wooBrands ?? [], $wooBrands);
                    }                
                    $page++;
                } while (!empty($wooBrandsChunk));
            } catch (\Exception $e) {
                static::$wooBrands = [];
            }
        }
        return static::$wooBrands;
    }

    /**
     * Return mapped product data
     * @return array
     */
    public function productData()
    {
        $netoProduct = $this->netoProduct;
        $name = $this->name();
        $salePrice = $this->salePrice();
        $brandId = $this->wooBrandId();
        $productData = [
            'name' => $name,
            'type' => $this->type(),
            'status' => $this->isEnabled() ? 'publish' : 'pending',
            'catalog_visibility' => $this->isEnabled() ? 'visible' : 'hidden',
            'description' => $this->description(),
            'short_description' => $this->shortDescription(),
            'sku' => $this->sku(),
            'regular_price' => $this->productPrice(),
            'sale_price' => $salePrice,
            'date_on_sale_from' => !empty($salePrice) ? $this->saleStart() : null,
            'date_on_sale_to' => !empty($salePrice) ? $this->saleEnd() : null,
            'tax_status' => 'taxable',
            'tax_class' => 'standard',
            'manage_stock' => true,
            'stock_quantity' => ArrayHelper::getValue($netoProduct, ['AvailableSellQuantity']),
            'weight' => ArrayHelper::getValue($netoProduct, ['ShippingWeight']),
            'dimensions' => [
                'length' => ArrayHelper::getValue($netoProduct, ['ShippingLength']),
                'width' => ArrayHelper::getValue($netoProduct, ['ShippingWidth']),
                'height' => ArrayHelper::getValue($netoProduct, ['ShippingHeight']),
            ],
            'shipping_class' => $this->shippingClass(),
            'categories' => $this->categories(),
            'images' => $this->images(),
            'attributes' => $this->attributes(),
            'upsell_ids' => $this->upsellProductsId(),
            'meta_data' => $this->metadata(),
            'brands' => !empty($brandId) ? [$brandId] : null,
            'product_brand' => !empty($brandId) ? [$brandId] : null,
        ];        
        return $productData;
    }

    /**
     * Return mapped product inventory data
     * @return array
     */
    public function inventoryData()
    {
        $inventoryData = $this->productData();
        unset($inventoryData['type']);
        unset($inventoryData['images']);
        return $inventoryData;
    }

    /**
     * Return mapped product variations data
     * @return array
     */
    public function productVariations()
    {
        $currentVariations = $this->wooVariations();
        $childProducts = $this->childProducts();
        $productVariations = [];
        foreach ($childProducts as $sku => $childProduct) {
            $wooProductId = ArrayHelper::getValue($currentVariations, [$sku, 'id']);
            $productMapper = new static($childProduct, $this->netoProduct, $wooProductId, $this->woo, $this->neto);
            $productData = $productMapper->productData();
            $productData['image'] = ArrayHelper::getValue($productData, ['images', 0]);
            if (!empty($wooProductId)) {                
                $productData['id'] = $wooProductId;
                $productVariations['update'][] = $productData;
            } else {
                $productVariations['create'][] = $productData;
            }
        }
        $childProductsSku = array_keys($childProducts);
        foreach ($currentVariations as $sku => $variation) {
            if (!in_array($sku, $childProductsSku)) {
                $productVariations['delete'][] = ArrayHelper::getValue($variation, ['id']);
            }
        }
        return $productVariations;
    }

    public function isUpdate()
    {
        return !empty($this->wooProductId);
    }

    /**
     * Return mapped neto product sku
     * @return string
     */
    public function sku()
    {
        $sku = ArrayHelper::getValue($this->netoProduct, ['SKU']);
        return $sku;
    }

    /**
     * Return mapped neto product name
     * @return string
     */
    public function name()
    {
        $name = ArrayHelper::getValue($this->netoProduct, ['Name']);
        return $name;
    }

    /**
     * Return mapped neto product descriptiom
     * @return string
     */
    public function description()
    {
        $description = ArrayHelper::getValue($this->netoProduct, ['Description']);
        if (empty($description)) {
            $description = $this->name();
        }                
        return $description;
    }

    /**
     * Return mapped neto product short descriptiom
     * @return string
     */
    public function shortDescription()
    {
        $shortDescription = ArrayHelper::getValue($this->netoProduct, ['ShortDescription']);
        if (empty($shortDescription)) {
            $shortDescription = $this->name();
        }                
        return $shortDescription;
    }

    /**
     * Return mapped neto product price
     * @return float
     */
    public function productPrice()
    {
        $price = ArrayHelper::getValue($this->netoProduct, ['RRP']);
        $price = empty(floatval($price)) ? null : $price;
        if (empty($price) && !empty($this->parentProduct)) {
            $price = ArrayHelper::getValue($this->parentProduct, ['RRP']);
            $price = empty(floatval($price)) ? null : $price;
        }
        return $price;
    }

    /**
     * Return mapped neto product sale price
     * @return float
     */
    public function salePrice()
    {
        $price = ArrayHelper::getValue($this->netoProduct, ['PromotionPrice']);
        return empty(floatval($price)) ? null : $price;
    }

    /**
     * Return mapped neto product sale start
     * @return float
     */
    public function saleStart()
    {
        $saleStart = null;
        $date = ArrayHelper::getValue($this->netoProduct, ['PromotionStartDate']);
        if (!empty($date) && $date !== '0000-00-00 00:00:00') {
            $saleStart = Carbon::parse($date)->format('Y-m-d\TH:i:s');
        }
        return $saleStart;
    }

    /**
     * Return mapped neto product sale end
     * @return float
     */
    public function saleEnd()
    {
        $saleEnd = null;
        $date = ArrayHelper::getValue($this->netoProduct, ['PromotionExpiryDate']);
        if (!empty($date) && $date !== '0000-00-00 00:00:00') {
            $saleEnd = Carbon::parse($date)->format('Y-m-d\TH:i:s');
        }
        return $saleEnd;
    }

    /**
     * Return woo product type
     * @return boolean
     */
    public function type()
    {
        $type = 'simple';
        $parentSku = ArrayHelper::getValue($this->netoProduct, ['ParentSKU']);
        if (!empty($parentSku)) {
            $type = 'variation';
        } elseif (!empty($this->childProducts())) {
            $type = 'variable';
        }
        return $type;
    }

    /**
     * Return Neto child product
     * @return array
     */
    public function childProducts()
    {
        if (is_null($this->childProducts)) {
            $data = [
                'Filter' => [
                    'ParentSKU' => $this->sku(),
                    'OutputSelector' => static::netoAttributes(),
                ]
            ];
            $result = $this->neto->sendRequest('GetItem', 'post', Json::encode($data));
            $childProducts = ArrayHelper::index(ArrayHelper::getValue($result, 'Item'), 'SKU');
            $this->childProducts = $childProducts;
        }
        return $this->childProducts;
    }

    /**
     * Return Neto upsell products sku
     * @return array
     */
    public function itemSpecifics()
    {
        $itemSpecifics = ArrayHelper::getValue($this->netoProduct, ['ItemSpecifics', 0, 'ItemSpecific']) ?? [];
        if (ArrayHelper::isAssociative($itemSpecifics)) {
            $itemSpecifics = [$itemSpecifics];
        }
        return $itemSpecifics;
    }

    /**
     * Return woo product attributes
     * @return array
     */
    public function wooAttributes()
    {
        if (is_null(static::$wooAttributes)) {
            $wooAttributesChunk = $this->woo->sendApiRequest(['v3/products/attributes']);
            $wooAttributes = ArrayHelper::index($wooAttributesChunk, function ($item) {
                $name = strtolower(ArrayHelper::getValue($item, ['name']));
                return $name;
            });
            static::$wooAttributes = array_merge(static::$wooAttributes ?? [], $wooAttributes);
        }        
        return static::$wooAttributes;
    }
    
    /**
     * Return product attributes
     * @return array
     */
    public function attributes()
    {
        $attributes = [];
        if ($this->type() == 'variable') {
            $childProducts = $this->childProducts();
            $options = [];
            foreach ($childProducts as $sku => $childProduct) {
                $productMapper = new static($childProduct, $this->netoProduct, null, $this->woo, $this->neto);
                $itemSpecifics = $productMapper->itemSpecifics();
                foreach ($itemSpecifics as $itemSpecific) {
                    $itemSpecificName = ArrayHelper::getValue($itemSpecific, ['Name']);                    
                    if (true || in_array($itemSpecificName, ['Colour', 'Size'])) {
                        $itemSpecificValue = ArrayHelper::getValue($itemSpecific, ['Value']);
                        $wooAttribute = ArrayHelper::getValue($this->wooAttributes(), strtolower($itemSpecificName));
                        if (empty($wooAttribute)) continue;
                        $options[strtolower($itemSpecificName)][] = $itemSpecificValue;
                    }
                }
            }            
            foreach ($options as $name => $values) {
                $wooAttribute = ArrayHelper::getValue($this->wooAttributes(), $name);
                $attributes[] =  [
                    'id' => $wooAttribute['id'],
                    'name' => $wooAttribute['name'],
                    'visible' => true,
                    'variation' => true,
                    'options' => array_values(array_unique($values)),
                ];
            }            
        } elseif ($this->type() == 'variation') {
            $itemSpecifics = $this->itemSpecifics();
            foreach ($itemSpecifics as $itemSpecific) {
                $itemSpecificName = ArrayHelper::getValue($itemSpecific, ['Name']);
                if (true || in_array($itemSpecificName, ['Colour', 'Size'])) {
                    $wooAttribute = ArrayHelper::getValue($this->wooAttributes(), strtolower($itemSpecificName));
                    if (empty($wooAttribute)) continue;
                    $itemSpecificValue = ArrayHelper::getValue($itemSpecific, ['Value']);
                    $attributes[] = [
                        'id' => $wooAttribute['id'],
                        'name' => $wooAttribute['name'],
                        'option' => $itemSpecificValue,
                    ];
                }
            }
        }
        return $attributes;
    }

    /**
     * Return Neto upsell products sku
     * @return array
     */
    public function upsellProductsSku()
    {
        $upsellProductsSku = [];
        $upsellProducts = ArrayHelper::getValue($this->netoProduct, ['UpsellProducts', 0, 'UpsellProduct']) ?? [];
        if (ArrayHelper::isAssociative($upsellProducts)) {
            $upsellProducts = [$upsellProducts];
        }
        if (!empty($upsellProducts)) {
            $upsellProductsSku = ArrayHelper::getColumn($upsellProducts, 'SKU');
        }        
        return $upsellProductsSku;
    }

    /**
     * Return upsell products id
     * @return array
     */
    public function upsellProductsId()
    {
        $upsellProductsSku = $this->upsellProductsSku();
        $upsellProductsId = [];
        if (!empty($upsellProductsSku)) {
            foreach ($upsellProductsSku as $upsellProductSku) {
                $wooProducts = $this->woo->sendApiRequest(['v3/products/', 'sku' => $upsellProductSku]);
                $upsellProductId = ArrayHelper::getValue($wooProducts, [0, 'id']);
                if (!empty($upsellProductId)) {
                    $upsellProductsId[] = $upsellProductId;
                }
            }
        }        
        return $upsellProductsId;
    }

    /**
     * Return woo product variations
     * @return array
     */
    public function wooVariations()
    {
        if (!empty($this->wooProductId) && is_null($this->wooVariations)) {            
            $wooVariations = $this->woo->sendApiRequest(['v3/products/' . $this->wooProductId . '/variations']);
            $wooVariations = ArrayHelper::index($wooVariations, 'sku');
            $this->wooVariations = $wooVariations;
        }
        return $this->wooVariations;
    }

    /**
     * check if product is enabled
     * @return boolean
     */
    public function isEnabled()
    {
        $productPrice = $this->productPrice();
        $isActive = ArrayHelper::getValue($this->netoProduct, ['IsActive']) == 'True';
        $isApproved = ArrayHelper::getValue($this->netoProduct, ['Approved']) == 'True';
        $isEnabled = $isActive && $isApproved && !empty($productPrice);
        return $isEnabled;
    }

    public function images()
    {
        $productImages = ArrayHelper::getValue($this->netoProduct, ['Images']);
        $images = [];
        foreach ($productImages as $productImage) {
            $url = ArrayHelper::getValue($productImage, ['URL']);
            $url .= '?ts=' . time();
            $images[] = [
                'src' => $url,
                'name' => ArrayHelper::getValue($productImage, ['Name']),
            ];
        }
        return $images;
    }

    public function metadata()
    {
        $metadata = [];
        
        $metadata[] = [
            'key' => 'product_features_desc',
            'value' => ArrayHelper::getValue($this->netoProduct, ['Features']),
        ];
        $metadata[] = [
            'key' => '_ebay_brand',
            'value' => $this->netoBrand(),
        ];
        $metadata[] = [
            'key' => '_yoast_wpseo_primary_product_brand',
            'value' => $this->wooBrandId(),
        ];        
        $metadata[] = [
            'key' => '_yoast_wpseo_focuskeywords',
            'value' => ArrayHelper::getValue($this->netoProduct, ['SEOMetaKeywords']),
        ];
        $metadata[] = [
            'key' => '_yoast_wpseo_metadesc',
            'value' => ArrayHelper::getValue($this->netoProduct, ['SEOMetaDescription']),
        ];
        $metadata[] = [
            'key' => '_yoast_wpseo_title',
            'value' => ArrayHelper::getValue($this->netoProduct, ['SEOPageTitle']),
        ];
        return $metadata;
    }

    public function netoBrand()
    {
        return ArrayHelper::getValue($this->netoProduct, ['Brand']);
    }

    public function wooBrandId()
    {
        $wooBrands = $this->wooBrands();
        $netoBrand = $this->netoBrand();
        $wooBrandId = ArrayHelper::getValue($wooBrands, [strtolower($netoBrand)]);
        /*
        if (empty($wooBrandId)) {
            $wooBrand = $this->woo->sendApiRequest('v3/products/brands', 'post', Json::encode(['name' => $netoBrand]));
            $wooBrandId = ArrayHelper::getValue($wooBrand, ['id']);
        }
        */
        return $wooBrandId;
    }

    public function shippingClass()
    {
        return 'flat_rate';
    }

    public function categories()
    {
        if ($this->type() == 'variation') {
            return [];
        }
        $categories = [];
        $wooCategories = $this->wooCategories();
        $categoriesName = [];
        foreach ($this->productCategoriesid() as $categoryId) {
            $categoriesName = ArrayHelper::merge($categoriesName, $this->productCategoryHierarchy($categoryId));
        }
        $prevCategoryId = null;
        foreach ($categoriesName as $categoryName) {
            $categoryId = ArrayHelper::getValue($wooCategories, [strtolower($categoryName)]);
            if (empty($categoryId)) {
                $categoryData = [
                    'parent' => $prevCategoryId, 
                    'name' => $categoryName
                ];
                try {
                    $wooCategory = $this->woo->sendApiRequest('v3/products/categories', 'post', Json::encode($categoryData));
                    $categoryId = ArrayHelper::getValue($wooCategory, ['id']);
                } catch (Exception $e) {
                    $exceptionResult = Json::decode($e->getMessage());
                    if (is_array($exceptionResult) && !empty($exceptionResult['data']['resource_id'])) {
                        $categoryId = $exceptionResult['data']['resource_id'];
                    }                    
                }                
                static::$wooCategories[strtolower($categoryName)] = $categoryId;
            // } elseif (!empty($prevCategoryId)) {
            //     $categoryData = [
            //         'parent' => $prevCategoryId, 
            //     ];
            //     $wooCategory = $this->woo->sendApiRequest('v3/products/categories/' . $categoryId, 'put', Json::encode($categoryData));
            }
            if (!empty($categoryId)) {
                $prevCategoryId = $categoryId;
                $categories[] = [
                    'id' => $categoryId
                ];
            }            
        }
        return $categories;
    }

    /**
     * Return product categories
     * @return array
     */
    protected function productCategories()
    {
        $categories = ArrayHelper::getValue($this->netoProduct, ['Categories']);
        if (ArrayHelper::isIndexed($categories)) {
            $categories = ArrayHelper::getValue($categories, [0, 'Category']);
        } else {
            $categories = ArrayHelper::getValue($categories, ['Category']);
        }
        if (!ArrayHelper::isIndexed($categories)) {
            $categories = [$categories];
        }
        return $categories;        
    }

    /**
     * Return product category hierarchy
     * @return array
     */
    protected function productCategoryHierarchy($categoryId)
    {
        $netoCategories = $this->netoCategories();
        $categoriesName = [];
        $categoryName = ArrayHelper::getValue($netoCategories, [$categoryId, 'CategoryName']);
        if (empty($categoryName)) {
            return $categoriesName;
        }
        $categoriesName[$categoryId] = $categoryName;
        $parentId = ArrayHelper::getValue($netoCategories, [$categoryId, 'ParentCategoryID']);
        if (!empty($parentId)) {
            $parentsName = $this->productCategoryHierarchy($parentId);
            if (!empty($parentsName)) {
                $parentsName = array_reverse($parentsName);
                $categoriesName = ArrayHelper::merge($categoriesName, $parentsName);
            }
        }
        $categoriesName = array_reverse($categoriesName, true);
        return $categoriesName;
    }

    /**
     * Return product categories id
     * @return array
     */
    protected function productCategoriesId()
    {
        $categories = $this->productCategories();
        $categoriesId = [];
        if (!empty($categories)) {
            $categoriesId = ArrayHelper::getColumn($categories, 'CategoryID');
        }        
        return $categoriesId;
    }

    /**
     * Return product categories name
     * @return array
     */
    protected function productCategoriesName()
    {
        $categories = $this->productCategories();
        $categoriesName = [];
        if (!empty($categories)) {
            $categoriesName = ArrayHelper::getColumn($categories, 'CategoryName');
        }
        return $categoriesName;
    }

    /**
     * Return product contents
     * @return array
     */
    protected function productContents()
    {
        $contents = ArrayHelper::getValue($this->netoProduct, ['RelatedContents']);
        if (ArrayHelper::isIndexed($contents)) {
            $contents = ArrayHelper::getValue($contents, [0, 'RelatedContent']);
        } else {
            $contents = ArrayHelper::getValue($contents, ['RelatedContent']);
        }
        if (!ArrayHelper::isIndexed($contents)) {
            $contents = [$contents];
        }
        return $contents;
    }

    /**
     * Return product contents id
     * @return array
     */
    protected function productContentsId()
    {
        $contents = $this->productContents();
        $contentsId = [];
        if (!empty($contents)) {
            $contentsId = ArrayHelper::getColumn($contents, 'ContentID');
        }        
        return $contentsId;
    }
}
