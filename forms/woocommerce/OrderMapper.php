<?php

namespace app\forms\woocommerce;

use app\components\Neto;
use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * OrderMapper woocommerce to neto order mapper
 */
class OrderMapper extends BaseObject
{
    /**
     * @var Neto
     */
    protected $neto;
    /**
     * @var array
     */
    protected $wooOrder;

    /**
     * @inheritdoc
     * @param Neto $neto
     * @param array $wooOrder woocommerce order
     */
    public function __construct($neto, $wooOrder, $config = array())
    {
        $this->neto = $neto;
        $this->wooOrder = $wooOrder;
        parent::__construct($config);
    }

    public function getWooOrder()
    {
        return $this->wooOrder;
    }

    /**
     * Return mapped order data
     * @return array
     */
    public function orderData()
    {
        $neto = $this->neto;
        $wooOrder = $this->wooOrder;
        $orderId = ArrayHelper::getValue($wooOrder, ['id']);
        $netoCustomer = $this->initNetoCustomer($neto, $wooOrder);
        if (empty($netoCustomer)) {
            echo date('c ') . $orderId . ' Neto Order Error: Neto customer is empty' . PHP_EOL;
            return null;            
        }        
        $purchaseOrderNumber = ArrayHelper::getValue($wooOrder, ['id']);
        $orderLines = [];
        $wooLines = ArrayHelper::getValue($wooOrder, ['line_items']);
        foreach ($wooLines as $wooLine) {
            $unitPrice = ArrayHelper::getValue($wooLine, ['price']) * 1.1;
            $orderLines[] = [
                'SKU' => ArrayHelper::getValue($wooLine, ['sku']),
                'Quantity' => ArrayHelper::getValue($wooLine, ['quantity']),
                'UnitPrice' => $unitPrice,
                'ExternalOrderReference' => $orderId,
            ];
        }
        $totalShippingCost = ArrayHelper::getValue($wooOrder, ['shipping_total']);
        $wooOrderDate = Carbon::parse(ArrayHelper::getValue($wooOrder, ['date_created']))->format('Y-m-d');
        $order = [
            'PurchaseOrderNumber' => $purchaseOrderNumber,
            'DatePlaced' => $wooOrderDate,
            'DateInvoiced' => $wooOrderDate,
            'OrderType' => 'sales',
            'OrderStatus' => $this->orderStatus(),
            'OrderApproval' => $this->isPaid(),
            'BillFirstName' => ArrayHelper::getValue($wooOrder, ['billing', 'first_name']),
            'BillLastName' => ArrayHelper::getValue($wooOrder, ['billing', 'last_name']),
            'BillCompany' => ArrayHelper::getValue($wooOrder, ['billing', 'company']),
            'BillStreet1' => ArrayHelper::getValue($wooOrder, ['billing', 'address_1']),
            'BillStreet2' => ArrayHelper::getValue($wooOrder, ['billing', 'address_2']),
            'BillCity' => ArrayHelper::getValue($wooOrder, ['billing', 'city']),
            'BillState' => ArrayHelper::getValue($wooOrder, ['billing', 'state']),
            'BillPostCode' => ArrayHelper::getValue($wooOrder, ['billing', 'postcode']),
            'BillCountry' => ArrayHelper::getValue($wooOrder, ['billing', 'country']),
            'BillContactPhone' => ArrayHelper::getValue($wooOrder, ['billing', 'phone']),
            'Email' => ArrayHelper::getValue($wooOrder, ['billing', 'email']),
            'ShippingCost' => number_format($totalShippingCost, 2, '.', ''),
            'ShipFirstName' => ArrayHelper::getValue($wooOrder, ['shipping', 'first_name']),
            'ShipLastName' => ArrayHelper::getValue($wooOrder, ['shipping', 'last_name']),
            'ShipCompany' => ArrayHelper::getValue($wooOrder, ['shipping', 'company']),
            'ShipStreet1' => ArrayHelper::getValue($wooOrder, ['shipping', 'address_1']),
            'ShipStreet2' => ArrayHelper::getValue($wooOrder, ['shipping', 'address_2']),
            'ShipCity' => ArrayHelper::getValue($wooOrder, ['shipping', 'city']),
            'ShipState' => ArrayHelper::getValue($wooOrder, ['shipping', 'state']),
            'ShipPostCode' => ArrayHelper::getValue($wooOrder, ['shipping', 'postcode']),
            'ShipCountry' => ArrayHelper::getValue($wooOrder, ['shipping', 'country']),
            'ShipContactPhone' => ArrayHelper::getValue($wooOrder, ['billing', 'phone']),
            'ShipInstructions' => null,
            'ShippingMethod' => $this->shippingMethodName(),
            'OrderLine' => $orderLines,
            'TaxInclusive' => true,
            'SalesChannel' => static::salesChannel(),
            'InternalOrderNotes' => 'WooCommerce Automated Order',
            'PaymentMethod' => $this->paymentMethodName(),
            'Username' => ArrayHelper::getValue($netoCustomer, ['Username']),
        ];
        return $order;
    }

    /**
     * Get/Create woo customer on neto
     * @return array
     */
    protected function initNetoCustomer($neto, $wooOrder)
    {
        $wooEmail = ArrayHelper::getValue($wooOrder, ['billing', 'email']);
        $customerResult = $neto->sendRequest('GetCustomer', 'post', Json::encode([
            'Filter' => [
                'Email' => $wooEmail,
                'OutputSelector' => [
                    'Username',
                    'EmailAddress',
                    'BillingAddress',
                    'ShippingAddress',
                ]
            ]
        ]));
        $netoCustomer = ArrayHelper::getValue($customerResult, ['Customer', 0]);
        if (empty($netoCustomer)) {
            $wooFirstname = ArrayHelper::getValue($wooOrder, ['billing', 'first_name']);
            $wooLastname = ArrayHelper::getValue($wooOrder, ['billing', 'last_name']);
            $customerResult = $neto->sendRequest('AddCustomer', 'post', Json::encode([
                'Customer' => [
                    'Username' => $wooEmail,
                    'Type' => 'Customer',
                    'EmailAddress' => $wooEmail,
                    'BillingAddress' => [
                        'BillFirstName' => $wooFirstname,
                        'BillLastName' => $wooLastname,
                        'BillCompany' => ArrayHelper::getValue($wooOrder, ['billing', 'company']),
                        'BillStreetLine1' => ArrayHelper::getValue($wooOrder, ['billing', 'address_1']),
                        'BillStreetLine2' => ArrayHelper::getValue($wooOrder, ['billing', 'address_2']),
                        'BillCity' => ArrayHelper::getValue($wooOrder, ['billing', 'city']),
                        'BillState' => ArrayHelper::getValue($wooOrder, ['billing', 'state']),
                        'BillPostCode' => ArrayHelper::getValue($wooOrder, ['billing', 'postcode']),
                        'BillCountry' => ArrayHelper::getValue($wooOrder, ['billing', 'country']),
                        'BillPhone' => ArrayHelper::getValue($wooOrder, ['billing', 'phone']),
                    ],
                    'ShippingAddress' => [
                        'ShipFirstName' => ArrayHelper::getValue($wooOrder, ['shipping', 'first_name']),
                        'ShipLastName' => ArrayHelper::getValue($wooOrder, ['shipping', 'last_name']),
                        'ShipCompany' => ArrayHelper::getValue($wooOrder, ['shipping', 'company']),
                        'ShipStreetLine1' => ArrayHelper::getValue($wooOrder, ['shipping', 'address_1']),
                        'ShipStreetLine2' => ArrayHelper::getValue($wooOrder, ['shipping', 'address_2']),
                        'ShipCity' => ArrayHelper::getValue($wooOrder, ['shipping', 'city']),
                        'ShipState' => ArrayHelper::getValue($wooOrder, ['shipping', 'state']),
                        'ShipPostCode' => ArrayHelper::getValue($wooOrder, ['shipping', 'postcode']),
                        'ShipCountry' => ArrayHelper::getValue($wooOrder, ['shipping', 'country']),
                        'ShipPhone' => ArrayHelper::getValue($wooOrder, ['billing', 'phone']),
                    ],
                ]
            ]));
            $customerResult = $neto->sendRequest('GetCustomer', 'post', Json::encode([
                'Filter' => [
                    'Email' => $wooEmail,
                    'OutputSelector' => [
                        'Username',
                        'EmailAddress',
                        'BillingAddress',
                        'ShippingAddress',
                    ]
                ]
            ]));
            $netoCustomer = ArrayHelper::getValue($customerResult, ['Customer', 0]);
        }        
        return $netoCustomer;
    }

    public static function salesChannel()
    {
        return 'woocommerce';
    }
    
    public function paymentMethodName()
    {
        return 'WooPayments';
    }

    public function shippingMethodName()
    {
        $wooShippingMethod =  ArrayHelper::getValue($this->wooOrder, ['shipping_lines', 0, 'method_title']);
        $shippingMethod = 'Flat Rate';
        if (stripos($wooShippingMethod, 'free') !== false) {
            $shippingMethod = 'Free Shipping';
        }
        return $shippingMethod;
    }    

    public function carrierName($shippingMethod)
    {
        return $shippingMethod;
    }

    public function orderStatus()
    {
        if (ArrayHelper::getValue($this->wooOrder, ['status']) == 'on-hold') {
            return 'On Hold';
        }
        return 'New';
    }

    public function isPaid()
    {
        return !empty(ArrayHelper::getValue($this->wooOrder, ['date_paid'])) && !empty($this->paymentMethodName());
    }
}
