<?php

namespace app\modules\wolfgroup\forms\woocommerce;

use app\components\Neto;
use Carbon\Carbon;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * D2uOrderMapper woocommerce to neto order mapper
 */
class D2uOrderMapper extends OrderMapper
{
    public static function salesChannel()
    {
        return 'WooCommerce'; //upto 15 chars
    }
    
    public function paymentMethodName()
    {
        return 'WooPayments';
    }

    public function shippingMethodName()
    {
        return '';
    }    

    public function carrierName($shippingMethod)
    {        
        $carrierName = null;
        if (stripos($shippingMethod, 'fastway') !== false || 
        in_array($shippingMethod, ['A4 Satchel (21.0 x 29.7cm)', 'A3 Satchel (29.7 x 42.0cm)', 'A2 Satchel (42.0 x 59.4cm)', 'Parcel'])) {
            $carrierName = 'Fastway AU';
        } elseif (stripos($shippingMethod, 'sendle') !== false) {
            $carrierName = 'Sendle';
        } elseif (stripos($shippingMethod, 'PARCEL POST + SIGNATURE') !== false) {
            $carrierName = 'AU Post';
        }
        return $carrierName;
    }

    public function orderStatus()
    {
        if (ArrayHelper::getValue($this->wooOrder, ['status']) == 'on-hold') {
            return 'On Hold';
        }
        return 'New';
    }

    public function isPaid()
    {
        return true;
    }
}
